<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    private $setup;

    /**
     * @param SetupService $setupService
     *
     */
    public function __construct()
    {
        $this->setup = null;
    }

    /**
     * @Route("/", name="default")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->render(
            'default/index.html.twig',
            [
                'controller_name' => ''
            ]
        );
    }
}
