<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MakeAuthController extends AbstractController
{
    /**
     * @Route("/make/auth", name="make_auth")
     */
    public function index()
    {
        dd($this->config);
        return $this->render('make_auth/index.html.twig', [
            'controller_name' => 'MakeAuthController',
        ]);
    }
}
